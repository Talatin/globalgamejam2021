﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GameIntro : MonoBehaviour
{
    [SerializeField] private TMP_Text bossText;
    [SerializeField] private string[] bossLines;
    private AudioSource source;
    private int Soundndex = 0;
    [SerializeField] private AudioClip[] VoicLines;
    int index = 0;
    // Start is called before the first frame update
    void Start()
    {
        bossText.text = bossLines[index];
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NextText()
    {
        index++;
        bossText.text = bossLines[index];
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
    }

    public void PlayHey()
    {
        source.clip = VoicLines[Soundndex];
        source.Play();
        Soundndex++;
    }

}
