﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MissingPack : MonoBehaviour
{
    public string Code;
    public Sprite image;
    public Sprite Character;
    public Color[] Colors;
    [SerializeField] private float timeTillRip;
    private Image timeIndicator;
    private float smolTimer = 0;
    public void setObject()
    {
        string temp = Code.Insert(2, " ");
        transform.GetChild(1).GetComponent<Image>().sprite = image;
        transform.GetChild(2).GetComponent<Image>().sprite = Character;
        transform.GetChild(3).GetComponent<TMP_Text>().text = temp;
        timeIndicator = transform.GetChild(5).GetComponent<Image>();
        timeIndicator.color = Colors[0];
    }

    private void Awake()
    {
        smolTimer = timeTillRip;
        
    }

    private void Update()
    {
        smolTimer -= Time.deltaTime;
        timeIndicator.fillAmount =  smolTimer / timeTillRip;
        if (timeIndicator.fillAmount < 0.5f && timeIndicator.fillAmount > 0.2f)
        {
            timeIndicator.color = Colors[1];
        }
        if (timeIndicator.fillAmount < 0.2f)
        {
            timeIndicator.color = Colors[2];
        }
        if (smolTimer <= 0)
        {
            smolTimer = timeTillRip;
            GameManager.instance.RemoveMissingPack(Code,false);
        }
        
    }
}
