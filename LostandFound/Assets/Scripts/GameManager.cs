﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Transform UiParent;
    public GameObject MissingPackUIPrefab;
    public List<GameObject> ALivePackages = new List<GameObject>();
    public List<GameObject> SearchedPackages = new List<GameObject>();
    public List<string> MissingPackageCodes = new List<string>();
    public List<GameObject> MissingListUI = new List<GameObject>();
    public LayerMask pLayer;
    public Image[] Lifes;
    public Sprite LifeGood;
    public Sprite LifeBad;
    private int health = 6;
    public GameObject GameOverObject;
    private bool gameIsOver;

    [SerializeField] private TruckBehavior truck;

    [SerializeField] private TMP_Text scoreText;
    private int score = 0;

    [Header("Spawning")]
    public GameObject PackagePrefab;
    public Sprite[] CharacterSprites;
    public Sprite[] PackageSprites;
    public Transform[] SpawnPos;
    [Space(5)]
    public float TimeTillDelivery;
    public float TimeTillAssignment;
    private float timerTD;
    private float timerTA;

    [Header("Sounds")]
    [SerializeField] private AudioClip NewMissingClip;
    [SerializeField] private AudioClip DestroyMissingClip;
    [SerializeField] private AudioClip SpawnClip;
    [SerializeField] private AudioClip GameOverClip;
    private AudioSource source;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
        timerTA = TimeTillAssignment - 1;
        source = GetComponent<AudioSource>();
        NewMissingPack();
    }

    

    void Update()
    {
        if (gameIsOver && Input.anyKey)
        {
            SceneManager.LoadScene(0);
        }
        HandleTimer();
    }

    private void HandleTimer()
    {
        timerTD += ALivePackages.Count > 15 ? Time.deltaTime : Time.deltaTime * 2;
        timerTA += MissingListUI.Count >= 3 ? Time.deltaTime : Time.deltaTime * 3;

        if (timerTA >= TimeTillAssignment)
        {
            timerTA = 0;
            if (MissingListUI.Count < 6)
            {
                NewMissingPack();

            }
        }
        if (timerTD >= TimeTillDelivery)
        {
            timerTD = 0;
            truck.SetAnim();
        }
    }


    public void SpawnNewPackage()
    {
        //int packAmount = Random.Range(2, 5);
        //foreach (var item in SpawnPos)
        //{
        //    if (!Physics2D.OverlapCircle(item.position, 0.2f) && packAmount > 0)
        //    {
        //        GameObject temp = Instantiate(PackagePrefab, item.position, Quaternion.identity);
        //        temp.GetComponent<SpriteRenderer>().sprite = PackageSprites[Random.Range(0, PackageSprites.Length)];
        //        packAmount--;
        //    }
        //}

        StartCoroutine(SpawnPackagesWithDelay());

    }

    private void NewMissingPack()
    {
        if (ALivePackages.Count > 0)
        {
            source.PlayOneShot(NewMissingClip);
            GameObject tempO = ALivePackages[Random.Range(0, ALivePackages.Count)];
            GameObject temp = Instantiate(MissingPackUIPrefab, UiParent);
            temp.GetComponent<MissingPack>().Code = tempO.GetComponent<PackageBehavior>().Code;
            temp.GetComponent<MissingPack>().image = tempO.GetComponent<SpriteRenderer>().sprite;
            temp.GetComponent<MissingPack>().Character = CharacterSprites[Random.Range(0, CharacterSprites.Length)];
            temp.GetComponent<MissingPack>().setObject();
            ALivePackages.Remove(tempO);
            SearchedPackages.Add(tempO);
            MissingPackageCodes.Add(tempO.GetComponent<PackageBehavior>().Code);

            MissingListUI.Add(temp);
        }
    }

    public void RemoveMissingPack(string code, bool successful)
    {
        foreach (var item in MissingListUI)
        {
            if (item.GetComponent<MissingPack>().Code == code)
            {
                MissingListUI.Remove(item);
                foreach (var pack in SearchedPackages)
                {
                    if (pack.GetComponent<PackageBehavior>().Code == code)
                    {
                        SearchedPackages.Remove(pack);
                        MissingPackageCodes.Remove(code);
                        break;
                    }
                }
                Destroy(item.gameObject);
                break;
            }
        }
        if (successful)
        {
            score++;
            scoreText.text = score.ToString();
        }
        else
        {
            source.PlayOneShot(DestroyMissingClip);
            health--;
            for (int i = 0; i < 6; i++)
            {
                if (i >= health)
                {
                    Lifes[i].sprite = LifeBad;
                }
                else
                {
                    Lifes[i].sprite = LifeGood;
                }
            }
            if (health <= 0)
            {
                GameOverObject.SetActive(true);
                Time.timeScale = 0f;
                gameIsOver = true;
                source.PlayOneShot(GameOverClip);
            }
        }

    }

    private void OnDrawGizmos()
    {
        //Gizmos.color = Color.red;
        //if (SpawnPos.Length != 0)
        //{
        //    foreach (var item in SpawnPos)
        //    {
        //        Gizmos.DrawWireSphere(item.position, 0.25f);
        //    }
        //}
    }

    IEnumerator SpawnPackagesWithDelay()
    {
        int packAmount = Random.Range(2, 5);
        foreach (var item in SpawnPos)
        {
            if (!Physics2D.OverlapCircle(item.position, 0.2f) && packAmount > 0 && !Physics2D.OverlapCircle(item.position, 1f, pLayer))
            {
                GameObject temp = Instantiate(PackagePrefab, item.position, Quaternion.identity);
                source.PlayOneShot(SpawnClip);
                temp.GetComponent<SpriteRenderer>().sprite = PackageSprites[Random.Range(0, PackageSprites.Length)];
                packAmount--;
                yield return new WaitForSeconds(0.35f);
            }
        }
    }

}
