﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RadioBehavior1 : MonoBehaviour
{
    [SerializeField] private Sprite[] StationRadios;
    [SerializeField] private AudioClip[] RadioStations;
    private AudioSource source;
    private SpriteRenderer spRend;
    private Animator anim;
    private int currentStation = 0;
    private int canUse;
    // Start is called before the first frame update
    void Start()
    {
        spRend = GetComponent<SpriteRenderer>();
        source = GetComponent<AudioSource>();
        anim = GetComponent<Animator>();
        anim.SetBool("Playing", true);
        source.clip = RadioStations[currentStation];
        source.Play();

    }

    // Update is called once per frame
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.RightShift) || Input.GetKeyDown(KeyCode.Return)) && canUse > 0)
        {
            currentStation++;
            if (currentStation >= RadioStations.Length)
            {
                currentStation = -1;
            }
            if (currentStation != -1)
            {
                source.clip = RadioStations[currentStation];
                spRend.sprite = StationRadios[currentStation];
                source.Play();
                anim.SetBool("Playing", true);
            }
            else
            {
                source.Stop();
                anim.SetBool("Playing", false);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        canUse++;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        canUse--;
    }


}
