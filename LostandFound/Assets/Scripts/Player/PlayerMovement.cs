﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    [Header("Inputs")]
    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Left;
    public KeyCode Right;

    public LayerMask layer;
    [HideInInspector] public Vector2 lastDir = new Vector2(1, 1);
    [HideInInspector] public bool moving;

    [SerializeField] private float speed;
    public Vector3 pos;
    public float InputDelay = 0.2f;
    private float delayTime = 0f;
    private float delaydelayTime = 0f;
    [SerializeField] private Animator anim;
    private enum faceDirection { left, up, right, down }
    private faceDirection faceDir = faceDirection.down;

    // Start is called before the first frame update
    void Start()
    {
        pos = transform.position;
    }

    private void Update()
    {
        anim.SetInteger("FaceDir", (int)faceDir);
        anim.SetBool("Moving", moving);
        Delay();

        if (delayTime <= InputDelay && transform.position == pos)
        {
            TurnPos();
        }
        else
        {
            MovePos();
        }

        if (transform.position == pos)
        {
            transform.position = pos;
            moving = false;
        }
        else
        {
            moving = true;
        }
        transform.position = Vector3.MoveTowards(transform.position, pos, Time.deltaTime * speed);
    }

    private void Delay()
    {
        if (Input.GetKey(Left) || Input.GetKey(Up) || Input.GetKey(Right) || Input.GetKey(Down))
        {
            delayTime += Time.deltaTime;
            delaydelayTime = 0;
        }
        else
        {
            delaydelayTime += Time.deltaTime * 2;
            if (delaydelayTime >= InputDelay)
            {
                delayTime = 0f;
            }
        }
    }
    private void TurnPos()
    {
        if (Input.GetKeyDown(Left))
        {
            if (lastDir == Vector2.left)
            {
                delayTime = InputDelay - 0.05f;
            }
            lastDir = Vector2.left;
            faceDir = faceDirection.left;
        }
        if (Input.GetKeyDown(Up))
        {
            if (lastDir == Vector2.up)
            {
                delayTime = InputDelay - 0.05f;
            }
            lastDir = Vector2.up;
            faceDir = faceDirection.up;
        }
        if (Input.GetKeyDown(Right))
        {
            if (lastDir == Vector2.right)
            {
                delayTime = InputDelay - 0.05f;
            }
            lastDir = Vector2.right;
            faceDir = faceDirection.right;
        }
        if (Input.GetKeyDown(Down))
        {
            if (lastDir == Vector2.down)
            {
                delayTime = InputDelay - 0.05f;
            }
            lastDir = Vector2.down;
            faceDir = faceDirection.down;
        }
    }

    private void MovePos()
    {
        if (Input.GetKey(Left) && transform.position == pos)
        {
            lastDir = Vector2.left;
            faceDir = faceDirection.left;
            if (!Physics2D.Raycast(transform.position + (Vector3)lastDir, Vector2.left, .49f, layer))
            {
                pos += Vector3.left;

            }
        }
        if (Input.GetKey(Up) && transform.position == pos)
        {
            lastDir = Vector2.up;
            faceDir = faceDirection.up;
            if (!Physics2D.Raycast(transform.position + (Vector3)lastDir, Vector2.up, .49f, layer))
            {
                pos += Vector3.up;
            }

        }
        if (Input.GetKey(Right) && transform.position == pos)
        {
            faceDir = faceDirection.right;
            lastDir = Vector2.right;
            if (!Physics2D.Raycast(transform.position + (Vector3)lastDir, Vector2.right, .49f, layer))
            {
                pos += Vector3.right;
            }
        }
        if (Input.GetKey(Down) && transform.position == pos)
        {
            lastDir = Vector2.down;
            faceDir = faceDirection.down;
            if (!Physics2D.Raycast(transform.position + (Vector3)lastDir, Vector2.down, .49f, layer))
            {
                pos += Vector3.down;
            }
        }
    }
}
