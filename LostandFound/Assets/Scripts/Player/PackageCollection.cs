﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PackageCollection : MonoBehaviour
{
    public KeyCode GrabInput;
    public KeyCode GrabInputSecondary;
    public Vector3 PackageHoldOffset;
    public Vector3 PackageSideOffset;
    private Vector3 packageOffset;
    private bool canDeliver = false;
    private PlayerSounds pSound;
    [SerializeField] private LayerMask packLayer;
    [SerializeField] private LayerMask packdropLayer;
    private PlayerMovement pMove;
    private GameObject currentPackage;
    [SerializeField] private TMP_Text SpeechBubble;
    [SerializeField] private Animator textAnim;
    // Start is called before the first frame update
    void Start()
    {
        pMove = GetComponent<PlayerMovement>();
        pSound = GetComponent<PlayerSounds>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(GrabInput) || Input.GetKeyDown(GrabInputSecondary))
        {
            if (currentPackage == null)
            {
                TakePackage();
            }
            else
            {
                DropPackage();
            }
        }

        if (currentPackage != null)
        {
            currentPackage.transform.position = transform.position + packageOffset;
            if (pMove.lastDir == Vector2.up)
            {
                currentPackage.GetComponent<SpriteRenderer>().sortingOrder = 0;
                packageOffset = PackageHoldOffset;
            }
            else if (pMove.lastDir == Vector2.left || pMove.lastDir == Vector2.right)
            {
                currentPackage.GetComponent<SpriteRenderer>().sortingOrder = 1;
                packageOffset = pMove.lastDir == Vector2.left ? new Vector3(-PackageSideOffset.x, PackageSideOffset.y, 0) : new Vector3(PackageSideOffset.x, PackageSideOffset.y, 0);
            }
            else
            {
                currentPackage.GetComponent<SpriteRenderer>().sortingOrder = 1;
                packageOffset = new Vector3(0, 0.4f, 0);
            }
        }
    }

    private void TakePackage()
    {
        ScanPackage();
        if (Physics2D.Raycast(transform.position + (Vector3)pMove.lastDir, pMove.lastDir, .49f, packLayer))
        {
            pSound.PlayPickupSound();
            currentPackage = Physics2D.Raycast(transform.position + (Vector3)pMove.lastDir, pMove.lastDir, .49f, packLayer).transform.gameObject;
            currentPackage.GetComponent<BoxCollider2D>().enabled = false;
            currentPackage.GetComponent<SpriteRenderer>().sortingOrder = 1;
        }
    }

    private void DropPackage()
    {
        if (!Physics2D.Raycast(transform.position + (Vector3)pMove.lastDir, pMove.lastDir, .49f, packdropLayer))
        {
            pSound.PlayDropDownSound();
            if (canDeliver && pMove.lastDir == Vector2.down)
            {
                string packCode = currentPackage.GetComponent<PackageBehavior>().Code;
                foreach (var code in GameManager.instance.MissingPackageCodes)
                {
                    if (packCode == code)
                    {
                        currentPackage.transform.position = transform.position + new Vector3(pMove.lastDir.x, pMove.lastDir.y);
                        currentPackage.GetComponent<PackageBehavior>().StartCountDown();
                        currentPackage = null;
                        return;
                    }
                }
            }
            currentPackage.transform.position = transform.position + new Vector3(pMove.lastDir.x, pMove.lastDir.y);
            currentPackage.transform.position = new Vector3(Mathf.RoundToInt(currentPackage.transform.position.x), Mathf.RoundToInt(currentPackage.transform.position.y));
            pMove.pos = new Vector3(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y));
            currentPackage.GetComponent<BoxCollider2D>().enabled = true;
            currentPackage.GetComponent<SpriteRenderer>().sortingOrder = 0;
            currentPackage = null;
        }
    }

    private bool ScanPackage()
    {
        if (Physics2D.Raycast(transform.position, pMove.lastDir, 1.49f, packLayer))
        {
            if (Physics2D.Raycast(transform.position, pMove.lastDir, 1.49f, packLayer).transform.TryGetComponent(out PackageBehavior packBehavior))
            {
                string packCode = packBehavior.Code;
                packCode = packCode.Insert(2, " ");
                SpeechBubble.text = packCode;
                textAnim.SetTrigger("SpeechBubble");
                foreach (var code in GameManager.instance.MissingPackageCodes)
                {
                    if (packCode == code)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeliveryZone"))
        {
            canDeliver = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("DeliveryZone"))
        {
            canDeliver = false;
        }
    }

}
