﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class PlayerSounds : MonoBehaviour
{
    [SerializeField] private AudioSource sourceFootSteps;
    [SerializeField] private AudioSource sourceFx;
    [SerializeField] private AudioClip[] footSteps;
    private int lastFootStepIndex = 0;
    [SerializeField] private AudioClip PickUp;
    [SerializeField] private AudioClip DropDown;
    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayFootStep()
    {
        int tempRand;
        do
        {
            tempRand = Random.Range(0, footSteps.Length);

        } while (tempRand == lastFootStepIndex);
        sourceFootSteps.PlayOneShot(footSteps[tempRand]);
        lastFootStepIndex = tempRand;
    }

    public void PlayPickupSound()
    {
        sourceFx.PlayOneShot(PickUp);
    }

    public void PlayDropDownSound()
    {
        sourceFx.PlayOneShot(DropDown);
    }
}
