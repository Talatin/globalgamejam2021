﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    public Image Background;
    public Sprite[] backGroundSprites;
    private int choice = -1;
    void Start()
    {
        Time.timeScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            choice = -1;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            choice = 1;
        }

        switch (choice)
        {
            case -1:
                Background.sprite = backGroundSprites[choice + 1];
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.RightControl))
                {
                    SceneManager.LoadScene(1);
                }

                break;
            case 1:

                Background.sprite = backGroundSprites[choice];
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.RightControl))
                {
                    SceneManager.LoadScene(2);
                }
                break;
            default:
                break;
        }


    }
    public void SetChoice(int value)
    {
        if (choice == value)
        {
            if (choice == -1)
            {
                SceneManager.LoadScene(1);
            }
            if (choice == 1)
            {
                SceneManager.LoadScene(2);
            }
        }
        choice = value;
    }
}
