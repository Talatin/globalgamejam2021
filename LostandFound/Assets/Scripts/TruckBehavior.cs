﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckBehavior : MonoBehaviour
{
    private Animator anim;
    private AudioSource source;
    public AudioClip clip;
    // Start is called before the first frame update
    void Awake()
    {
        anim = GetComponent<Animator>();
        source = GetComponent<AudioSource>();
        source.clip = clip;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetSound()
    {
        if (!source.isPlaying)
        {
            source.Play();
        }

    }
    public void SetAnim()
    {
        anim.SetTrigger("TruckStart");
    }

    public void CallSpawnStart()
    {
        GameManager.instance.SpawnNewPackage();
    }

}
