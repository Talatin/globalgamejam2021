﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PackageBehavior : MonoBehaviour
{
    public AudioClip Poof;
    public string Code;
    char[] alphabet;
    private AudioSource source;
    // Start is called before the first frame update
    void Awake()
    {
        alphabet = "abcdefghijklmnopqrstuvwxyz".ToUpper().ToCharArray();
        GenerateCode();
        source = GetComponent<AudioSource>();
    }

    private void Start()
    {
        GameManager.instance.ALivePackages.Add(this.gameObject);
        
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void GenerateCode()
    {
        Code += alphabet[Random.Range(0, 26)];
        Code += alphabet[Random.Range(0, 26)];
        Code += Random.Range(0, 10);
        Code += Random.Range(0, 10);
        Code += Random.Range(0, 10);
    }

    public void StartCountDown()
    {
        StartCoroutine(CountDown());
    }
    
    public void SelfDestruct()
    {
       
        Destroy(gameObject);
    }

    IEnumerator CountDown()
    {
        GameManager.instance.MissingPackageCodes.Remove(Code);
        GameManager.instance.RemoveMissingPack(Code, true);
        yield return new WaitForSeconds(1);
        GetComponent<Animator>().enabled = true;
        source.PlayOneShot(Poof);
        GetComponent<Animator>().SetTrigger("Destroy");
    }
}
